# TYPO3 example ci

# Todo

* [ ] add sample for different QA tools
* [ ] add docs for the gitlab ci tasks
* [ ] split into 2 ways and separate pipelines:
  * use autodevops example
  * use standalone tools
  * https://docs.gitlab.com/ee/ci/parent_child_pipelines.html
