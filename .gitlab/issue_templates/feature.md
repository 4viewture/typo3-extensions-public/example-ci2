## Zusammenfassung

Kurze Zusammenfassung ihres Wunsches

## Schritte zum Reproduzieren

* Welches URL benötigt das Feature?
* Welche Schritte wurden durchgeführt? (URL, Datensätze)

## Welches Verhalten wurde erwartet?

Was sollte statt des eingetretenen passieren?

Hier bitte möglichst genau beschreiben, was gewünscht ist.

## Logdateien oder Screenshots

* Bitte hier Entwürfe oder Screenshots ablegen (Firefox rechtsklick, Bildschirmfoto).
* Logdateien bitte mit (```) umschließen, damit die leicher lesbar sind

# Externer Kontakt

* Falls extern gemeldet - wer (E-Mail) hat den Fehler gemeldet
