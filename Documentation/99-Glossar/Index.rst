.. include:: /Includes.rst.txt
.. index:: Nachschlagen; Glossar
.. _Glossar:

============
Glossar
============


.. http://www.sphinx-doc.org/en/master/usage/restructuredtext/directives.html?highlight=glossar#directive-glossary

.. glossary::

   Cache
      Zwischenpuffer für Ergebnisse von langsamen Algorithmen.
      Im Kontext von :term:`CMS` Systemen dient der Cache primär der beschleunigung des Frontends.

   CMS
      Content management system

   Cookie
      Datenpaket, dass auf dem Rechner gespeichert wird, siehe :ref:`cookie`

   Datenschutzgrundverordnung
      siehe :term:`DSGVO`

   DSGVO
      Datenschutzgrundverordnung mehr siehe auch :ref:`cookie`

   SEO
      siehe :term:`Suchmaschinenoptimierung`

   Suchmaschinenoptimierung
      Beschreibt das Anpassen der Inhalte, sodass diese für Suchmaschinen und damit für Menschen besser zugänglich sind.
      :ref:`seo`

   TYPO3
      TYPO3 ist eine Webseiten Content Management System (:term:`CMS`).
      …


.. tip::

   Man kann auf Definitionen im Glossar einfach linken. Beispiel::

      TYPO3 ist ein :term:`cms`

   Wird zu: TYPO3 ist ein :term:`CMS`
