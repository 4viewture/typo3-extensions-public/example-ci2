.. include:: /Includes.rst.txt

===========================================
Documentation Sample
===========================================

:Autoren:   Kay Strobach (4viewture UG)
:E-Mail:    Kay Strobach <support@4viewture.eu>
:Lizenz:    Nur zur internen Verwendung
:Stand:     Oktober 2019
:Generiert: |today|

.. rubric:: Inhalt

.. toctree::
   :maxdepth: 3
   :caption: Allgemein

   00-Einleitung/Index

.. toctree::
   :maxdepth: 3
   :caption: Für Redakteure

   10-Anleitungen/Index

.. toctree::
   :maxdepth: 3
   :caption: Webauftritt betreiben

   20-Ops/Index
   25-Dienstleister/Index

.. toctree::
   :maxdepth: 3
   :caption: Entwickler

   50-Modernisierungen/Index

.. toctree::
   :maxdepth: 3
   :caption: Über

   90-META/Index
   91-Todo/Index
   99-Glossar/Index
   genindex
