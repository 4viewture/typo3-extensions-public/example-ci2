.. include:: /Includes.rst.txt
.. _todo:

=====
Todos
=====

Liste mit Todos
===============

.. todolist::

Verwendung
==========

Hier werden alle Todo Einträge aus der Dokumentation gesammelt.

.. code-block:: rst

    .. todo::

        Use this directive like, for example, note.
