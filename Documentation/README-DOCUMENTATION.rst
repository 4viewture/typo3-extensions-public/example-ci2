:orphan:

.. include:: /Includes.rst.txt
.. _README-DOCUMENTATION:

==========================================
README-DOCUMENTATION.rst
==========================================

Render-Kommando::

   # gehe in Projekt-Startverzeichnis
   cd typo3-main-typo3-main-distributiondistribution

   # gehe NICHT nach
   # typo3-main-typo3-main-distributiondistribution/Documentation

   # create result folder
   mkdir -p Documentation-GENERATED-temp

   # run Docker
   docker run --rm \
      -v $(pwd):/PROJECT:ro \
      -v $(pwd)/Documentation-GENERATED-temp:/RESULT \
      t3docs/render-documentation:develop \
      makeall -c jobfile /PROJECT/Documentation/jobfile.json
