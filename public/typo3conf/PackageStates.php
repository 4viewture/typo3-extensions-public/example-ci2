<?php
# PackageStates.php

# This file is maintained by TYPO3's package management. Although you can edit it
# manually, you should rather use the extension manager for maintaining packages.
# This file will be regenerated automatically if it doesn't exist. Deleting this file
# should, however, never become necessary if you use the package commands.

return [
    'packages' => [
        'core' => [
            'packagePath' => 'typo3/sysext/core/',
        ],
        'scheduler' => [
            'packagePath' => 'typo3/sysext/scheduler/',
        ],
        'extbase' => [
            'packagePath' => 'typo3/sysext/extbase/',
        ],
        'fluid' => [
            'packagePath' => 'typo3/sysext/fluid/',
        ],
        'frontend' => [
            'packagePath' => 'typo3/sysext/frontend/',
        ],
        'fluid_styled_content' => [
            'packagePath' => 'typo3/sysext/fluid_styled_content/',
        ],
        'filelist' => [
            'packagePath' => 'typo3/sysext/filelist/',
        ],
        'impexp' => [
            'packagePath' => 'typo3/sysext/impexp/',
        ],
        'form' => [
            'packagePath' => 'typo3/sysext/form/',
        ],
        'install' => [
            'packagePath' => 'typo3/sysext/install/',
        ],
        'info' => [
            'packagePath' => 'typo3/sysext/info/',
        ],
        'linkvalidator' => [
            'packagePath' => 'typo3/sysext/linkvalidator/',
        ],
        'recordlist' => [
            'packagePath' => 'typo3/sysext/recordlist/',
        ],
        'backend' => [
            'packagePath' => 'typo3/sysext/backend/',
        ],
        'recycler' => [
            'packagePath' => 'typo3/sysext/recycler/',
        ],
        'reports' => [
            'packagePath' => 'typo3/sysext/reports/',
        ],
        'setup' => [
            'packagePath' => 'typo3/sysext/setup/',
        ],
        'rte_ckeditor' => [
            'packagePath' => 'typo3/sysext/rte_ckeditor/',
        ],
        'about' => [
            'packagePath' => 'typo3/sysext/about/',
        ],
        'adminpanel' => [
            'packagePath' => 'typo3/sysext/adminpanel/',
        ],
        'belog' => [
            'packagePath' => 'typo3/sysext/belog/',
        ],
        'beuser' => [
            'packagePath' => 'typo3/sysext/beuser/',
        ],
        'extensionmanager' => [
            'packagePath' => 'typo3/sysext/extensionmanager/',
        ],
        'felogin' => [
            'packagePath' => 'typo3/sysext/felogin/',
        ],
        'filemetadata' => [
            'packagePath' => 'typo3/sysext/filemetadata/',
        ],
        'lowlevel' => [
            'packagePath' => 'typo3/sysext/lowlevel/',
        ],
        'redirects' => [
            'packagePath' => 'typo3/sysext/redirects/',
        ],
        'seo' => [
            'packagePath' => 'typo3/sysext/seo/',
        ],
        'sys_note' => [
            'packagePath' => 'typo3/sysext/sys_note/',
        ],
        't3editor' => [
            'packagePath' => 'typo3/sysext/t3editor/',
        ],
        'tstemplate' => [
            'packagePath' => 'typo3/sysext/tstemplate/',
        ],
        'viewpage' => [
            'packagePath' => 'typo3/sysext/viewpage/',
        ],
        'beuser_fastswitch' => [
            'packagePath' => 'typo3conf/ext/beuser_fastswitch/',
        ],
        'cms_fluid_precompiler_module' => [
            'packagePath' => 'typo3conf/ext/cms_fluid_precompiler_module/',
        ],
        'dashboard' => [
            'packagePath' => 'typo3conf/ext/dashboard/',
        ],
        'ddev_utilities' => [
            'packagePath' => 'typo3conf/ext/ddev_utilities/',
        ],
        'extension_builder' => [
            'packagePath' => 'typo3conf/ext/extension_builder/',
        ],
        'extractor' => [
            'packagePath' => 'typo3conf/ext/extractor/',
        ],
        'filefill' => [
            'packagePath' => 'typo3conf/ext/filefill/',
        ],
        'frontend_editing' => [
            'packagePath' => 'typo3conf/ext/frontend_editing/',
        ],
        'func' => [
            'packagePath' => 'typo3conf/ext/func/',
        ],
        'gdpr' => [
            'packagePath' => 'typo3conf/ext/gdpr/',
        ],
        'image_autoresize' => [
            'packagePath' => 'typo3conf/ext/image_autoresize/',
        ],
        'imageoptimizer' => [
            'packagePath' => 'typo3conf/ext/imageoptimizer/',
        ],
        'ke_search' => [
            'packagePath' => 'typo3conf/ext/ke_search/',
        ],
        'migrate_redirects' => [
            'packagePath' => 'typo3conf/ext/migrate_redirects/',
        ],
        'page_speed_insights' => [
            'packagePath' => 'typo3conf/ext/page_speed_insights/',
        ],
        'querybuilder' => [
            'packagePath' => 'typo3conf/ext/querybuilder/',
        ],
        'staticfilecache' => [
            'packagePath' => 'typo3conf/ext/staticfilecache/',
        ],
        'yoast_seo' => [
            'packagePath' => 'typo3conf/ext/yoast_seo/',
        ],
    ],
    'version' => 5,
];
