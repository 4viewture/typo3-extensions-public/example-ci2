<?php


$EM_CONF[$_EXTKEY] = [
    'title' => 'example_extension',
    'description' => 'example_extension',
    'category' => 'plugin',
    'author' => 'Kay Strobach',
    'author_email' => 'ks@4viewture.eu',
    'author_company' => '4viewture',
    'state' => 'beta',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '9.5.2',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.0-9.5.99',
        ],
        'conflicts' => [],
        'suggests' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'FourViewture\\ExampleExtension\\' => 'Classes',
        ],
    ]
];
