# Extension directory

Place all the extensions which are extension specific into this folder.

This directory is used as composer repository via the following line in the `composer.json` file.

```json
{"type": "path", "url": "DistributionPackages/*/"}
```
